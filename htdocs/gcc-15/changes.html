<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>GCC 15 Release Series &mdash; Changes, New Features, and Fixes</title>
<link rel="stylesheet" type="text/css" href="https://gcc.gnu.org/gcc.css">
</head>

<!-- GCC maintainers, please do not hesitate to contribute/update
     entries concerning those part of GCC you maintain!
-->

<body>
<h1>GCC 15 Release Series<br>Changes, New Features, and Fixes</h1>

<p>
This page is a "brief" summary of some of the huge number of improvements
in GCC 15.
You may also want to check out our
<a href="porting_to.html">Porting to GCC 15</a> page <!-- and the
<a href="../onlinedocs/index.html#current">full GCC documentation</a>.
-->
</p>

<p>Note: GCC 15 has not been released yet, so this document is
a work-in-progress.</p>

<!-- .................................................................. -->
<h2>Caveats</h2>
<ul>
  <li>Support for Nios II targets, which was marked obsolete in GCC 14,
    has now been removed entirely.
  </li>
  <li>In the AArch64 port, support for ILP32 (<code>-mabi=ilp32</code>) has
    been deprecated and will be removed in a future release.
  </li>
  <li><code>{0}</code> initializer in C or C++ for unions no longer
    guarantees clearing of the whole union (except for static storage
    duration initialization), it just initializes the first
    union member to zero.  If initialization of the whole union including
    padding bits is desirable, use <code>{}</code> (valid in C23 or C++)
    or use <code>-fzero-init-padding-bits=unions</code> option to restore
    old GCC behavior.</li>
</ul>


<!-- .................................................................. -->
<h2 id="general">General Improvements</h2>

<ul>
  <li>The default vectorizer cost model at <code>-O2</code> has been enhanced
    to handle unknown tripcount. But it still disables vectorization of loops
    when any runtime check for data dependence or alignment is required,
    it also disables vectorization of epilogue loops but otherwise is equal
    to the cheap cost model.
  </li>
  <li><code>-ftime-report</code> now only reports monotonic run time instead of
    system and user time. This reduces the overhead of the option significantly,
    making it possible to use in standard build systems.
  </li>
</ul>


<!-- .................................................................. -->
<h2 id="languages">New Languages and Language specific improvements</h2>

<h3 id="openmp">OpenMP</h3>

<p>
  See the
  <a href="../projects/gomp/">GNU Offloading and Multi-Processing Project (GOMP)</a>
  page for general information.
</p>

<ul>
  <li>
    Support for unified-shared memory has been added for some AMD and Nvidia
    GPU devices, enabled when using the <code>unified_shared_memory</code>
    clause to the <code>requires</code> directive.
    The OpenMP 6.0 <code>self_maps</code> clause is also now supported.
    For details,
    see the offload-target specifics section in the
    <a href="https://gcc.gnu.org/onlinedocs/libgomp/Offload-Target-Specifics.html"
       >GNU Offloading and Multi Processing Runtime Library Manual</a>.
  </li>
  <li>
    GCC added <code>ompx_gnu_pinned_mem_alloc</code> as a <a
    href="https://gcc.gnu.org/onlinedocs/libgomp/OMP_005fALLOCATOR.html">predefined
      allocator</a>.
  </li>
  <li>
    In C and Fortran, the <code>allocate</code> directive now supports
    static variables; stack variables were previously supported in
    those languages.  C++ support is not available yet.
  </li>
  <li>
    Offloading improvements:
    On <a href="https://gcc.gnu.org/onlinedocs/libgomp/nvptx.html">Nvidia
    GPUs, writing to the terminal</a> from OpenMP target regions (but not from
    OpenACC compute regions) is now also supported in Fortran; in C/C++ and
    on AMD GPUs this was already supported before with both OpenMP and OpenACC.
    Constructors and destructors on the device side for
    <code>declare target</code> static aggregates are now handled.
  </li>
  <li>
    The OpenMP 5.1 <code>unroll</code> and <code>tile</code>
    loop-transforming constructs are now supported.
  </li>
  <li>OpenMP 5.0 metadirectives are now supported, as are OpenMP 5.1 dynamic
    selectors in both <code>metadirective</code> and
    <code>declare variant</code> (the latter with some restrictions).
  </li>
  <li>
    The OpenMP 5.1 <code>dispatch</code> construct has been implemented
    with support for the <code>adjust_args</code> clause to the
    <code>declare variant</code> directive.
  </li>
  <li>
    OpenMP 6.0: The <a
    href="https://gcc.gnu.org/onlinedocs/libgomp/omp_005fget_005fdevice_005ffrom_005fuid.html"
    ><code>get_device_from_uid</code></a> and <a
    href="https://gcc.gnu.org/onlinedocs/libgomp/omp_005fget_005fuid_005ffrom_005fdevice.html">
    <code>omp_get_uid_from_device</code></a> API routines have been added.
  </li>
</ul>

<!-- <h3 id="ada">Ada</h3> -->

<h3 id="c-family">C family</h3>
<ul>
    <li>A <a href="https://gcc.gnu.org/onlinedocs/gcc/Statement-Attributes.html#index-musttail-statement-attribute">
        <code>musttail</code> statement attribute</a> was added to enforce tail calls.</li>
    <li><a href="https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html"
	   >Extended</a> inline assembler statements
      can now be used with some limitations outside of functions as well.
      New constraints have been added for defining symbols or using symbols
      inside of inline assembler, and a new generic operand modifier has
      been added to allow printing those regardless of PIC.  For example:
<pre>
struct S { int a, b, c; };
extern foo (void);
extern char var;
int var2;
asm (".text; %cc0: mov %cc2, %%r0; .previous;"
     ".rodata: %cc1: .byte %3; .previous" : :
     ":" (foo), /* Tell compiler asm defines foo function. */
     ":" (&amp;var), /* Tell compiler asm defines var variable.  */
     "-s" (var2), /* Tell compiler asm uses var2 variable.  */
                  /* "s" would work too but might not work with -fpic.  */
     "i" (sizeof (struct S))); /* It is possible to pass constants to toplevel asm.  */
</pre>
        </li>
    <li>The <code>"redzone"</code> clobber is now allowed in inline
        assembler statements to describe that the assembler can overwrite
        memory in the stack red zone (e.g. on x86-64 or PowerPC).</li>
    <li>The <a href="https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-nonnull_005fif_005fnonzero-function-attribute">
        nonnull_if_nonzero</a> function attribute has been added to describe
        functions where some pointer parameter may be <code>NULL</code> only
        if some other parameter is zero.</li>
    <li>The <a href="https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wtrailing-whitespace_003d">
        <code>-Wtrailing-whitespace=</code></a> and
        <a href="https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wleading-whitespace_003d">
        <code>-Wleading-whitespace=</code></a> options have been added to
        diagnose certain whitespace characters at the end of source lines or
        whitespace characters at the start of source lines violating certain
        indentation styles.</li>
    <li>The <a href="https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wheader-guard">
        <code>-Wheader-guard</code></a> warning has been added and enabled
        in <code>-Wall</code> to warn about some inconsistencies in header
        file guarding macros.</li>
</ul>

<h3 id="c">C</h3>
<ul>
  <li>C23 by default: GCC 15 changes the default language version
    for C compilation from
    <a href="https://gcc.gnu.org/onlinedocs/gcc/C-Dialect-Options.html#index-std-1">-std=gnu17</a>
    to
    <a href="https://gcc.gnu.org/onlinedocs/gcc/C-Dialect-Options.html#index-std-1">-std=gnu23</a>.
    If your code relies on older versions of the C standard, you will need to
    either add
    <a href="https://gcc.gnu.org/onlinedocs/gcc/C-Dialect-Options.html#index-std-1">-std=</a>
    to your build flags, or port your code; see <a href="porting_to.html#c23">the porting notes</a>.
  <li>Some more C23 features have been implemented:
    <ul>
      <li><code>#embed</code> preprocessing directive support.</li>
      <li>Support for <code>unsequenced</code> and <code>reproducible</code>
      attributes.</li>
      <li><code>__STDC_VERSION__</code> predefined macro value changed
      for <code>-std=c23</code> or <code>-std=gnu23</code> to
      <code>202311L</code>.</li>
    </ul>
  </li>
  <li>Some new features from the upcoming C2Y revision of the ISO C
  standard are supported with <code>-std=c2y</code>
  and <code>-std=gnu2y</code>.  Some of these features are also
  supported as extensions when compiling for older language versions.
    <ul>
      <li>Generic selection expression with a type operand.</li>
      <li>Support <code>++</code> and <code>--</code> on complex values.</li>
      <li>Accessing byte arrays.</li>
      <li><code>alignof</code> of an incomplete array type.</li>
      <li>Obsolete implicitly octal literals and add delimited escape
      sequences (just partially implemented, support for new syntax added
      but nothing deprecated yet).</li>
      <li>Named loops.</li>
      <li>More Modern Bit Utilities (addition of
      <code>__builtin_stdc_rotate_left</code> and
      <code>__builtin_stdc_rotate_right</code> builtins for use in future
      C library <code>&lt;stdbit.h&gt;</code> headers).</li>
      <li>Case range expressions.</li>
      <li><code>if</code> declarations.</li>
      <li>Introduce complex literals.</li>
      <li>Abs Without Undefined Behavior (addition of builtins for
      use in future C library <code>&lt;stdlib.h&gt;</code> headers).</li>
      <li>Allow zero length operations on null pointers (just the compiler
      side, C library headers will need adjustments too if using
      <code>nonnull</code> attribute).</li>
    </ul>
  </li>
</ul>

<h3 id="cxx">C++</h3>

<ul>
  <li>Several C++26 features have been implemented:
    <ul>
      <li><a href="https://wg21.link/P2558R2">P2558R2</a>, Add @, $, and ` to the basic
      character set (<a href="https://gcc.gnu.org/PR110343">PR110343</a>)
      </li>
      <li><a href="https://wg21.link/P2552R3">P2552R3</a>, On the ignorability of
      standard attributes (<a href="https://gcc.gnu.org/PR110345">PR110345</a>)
      </li>
      <li><a href="https://wg21.link/P2662R3">P2662R3</a>, Pack indexing
      (<a href="https://gcc.gnu.org/PR113798">PR113798</a>)
      </li>
      <li><a href="https://wg21.link/P0609R3">P0609R3</a>, Attributes for structured
      bindings (<a href="https://gcc.gnu.org/PR114456">PR114456</a>)
      </li>
      <li><a href="https://wg21.link/P2573R2">P2573R2</a>,
      <code>= delete("reason");</code> (<a href="https://gcc.gnu.org/PR114458">PR114458</a>)
      </li>
      <li><a href="https://wg21.link/P2893R3">P2893R3</a>, Variadic friends
      (<a href="https://gcc.gnu.org/PR114459">PR114459</a>)
      </li>
      <li><a href="https://wg21.link/P3034R1">P3034R1</a>, Disallow module declarations
      to be macros (<a href="https://gcc.gnu.org/PR114461">PR114461</a>)
      </li>
      <li><a href="https://wg21.link/P2747R2">P2747R2</a>, <code>constexpr</code>
      placement new (<a href="https://gcc.gnu.org/PR115744">PR115744</a>)
      </li>
      <li><a href="https://wg21.link/P0963R3">P0963R3</a>, Structured binding declaration
      as a condition (<a href="https://gcc.gnu.org/PR115745">PR115745</a>)
      </li>
      <li><a href="https://wg21.link/P3144R2">P3144R2</a>, Deleting a pointer to an
      incomplete type should be ill-formed
      (<a href="https://gcc.gnu.org/PR115747">PR115747</a>)
      </li>
      <li><a href="https://wg21.link/P3176R0">P3176R0</a>, Oxford variadic comma
      (<a href="https://gcc.gnu.org/PR117786">PR117786</a>)
      </li>
      <li><a href="https://wg21.link/P2865R5">P2865R5</a>, Removing deprecated array
      comparisons (<a href="https://gcc.gnu.org/PR117788">PR117788</a>)
      </li>
      <li><a href="https://wg21.link/P1967R14">P1967R14</a>, <code>#embed</code>
      (<a href="https://gcc.gnu.org/PR119065">PR119065</a>)
      </li>
      <li><a href="https://wg21.link/P3247R2">P3247R2</a>, Deprecating the notion of
      trivial types (<a href="https://gcc.gnu.org/PR117787">PR117787</a>)
      </li>
    </ul>
  </li>
  <li>Several C++23 features have been implemented:
    <ul>
      <li><a href="https://wg21.link/P2615R1">P2615R1</a>, Meaningful exports
      (<a href="https://gcc.gnu.org/PR107688">PR107688</a>)
      </li>
      <li><a href="https://wg21.link/P2718R0">P2718R0</a>, Wording for P2644R1
      Fix for Range-based for Loop
      (<a href="https://gcc.gnu.org/PR107637">PR107637</a>)
      </li>
    </ul>
  </li>
  <li>Several C++ Defect Reports have been resolved, e.g.:
    <ul>
      <li><a href="https://wg21.link/cwg36">DR 36</a>,
      <em>using-declarations</em> in multiple-declaration contexts</li>
      <li><a href="https://wg21.link/cwg882">DR 882</a>,
      Defining main as deleted</li>
      <li><a href="https://wg21.link/cwg1363">DR 1363</a>,
      Triviality vs multiple default constructors</li>
      <li><a href="https://wg21.link/cwg1496">DR 1496</a>,
      Triviality with deleted and missing default constructors</li>
      <li><a href="https://wg21.link/cwg2387">DR 2387</a>,
      Linkage of const-qualified variable template</li>
      <li><a href="https://wg21.link/cwg2521">DR 2521</a>,
      User-defined literals and reserved identifiers</li>
      <li><a href="https://wg21.link/cwg2627">DR 2627</a>,
      Bit-fields and narrowing conversions</li>
      <li><a href="https://wg21.link/cwg2819">DR 2819</a>,
      Cast from null pointer value in a constant expression (C++26 only)</li>
      <li><a href="https://wg21.link/cwg2867">DR 2867</a>,
      Order of initialization for structured bindings</li>
      <li><a href="https://wg21.link/cwg2918">DR 2918</a>,
      Consideration of constraints for address of overloaded function</li>
    </ul>
  </li>
  <li>Inline assembler statements now support
    <a href="https://gcc.gnu.org/onlinedocs/gcc/asm-constexprs.html"><code>constexpr</code> generated strings</a>,
    analoguous to <code>static_assert</code>.</li>
  <li>
    <!-- commit r15-2117-g313afcfdabeab3 -->
    Qualified name lookup failure into the current instantiation, e.g.
    <code>this-&gt;non_existent</code>, is now proactively diagnosed
    when parsing a template.
  </li>
  <li>The <a href="https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Dialect-Options.html#index-fassume-sane-operators-new-delete">
    <code>-fassume-sane-operators-new-delete</code></a> option has been
    added and enabled by default.  This option allows control over some
    optimizations around calls to replaceable global operators new and
    delete.  If a program overrides those replaceable global operators and
    the replaced definitions read or modify global state visible to the
    rest of the program, programs might need to be compiled with
    <code>-fno-assume-sane-operators-new-delete</code>.</li>
  <li>
    <!-- commit r15-871-gefaaae49b307fc -->
    The <a href="https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html#index-Wself-move">
        <code>-Wself-move</code></a> warning now warns even in a
	member-initializer-list.
  </li>
  <li>
    <!-- commit r15-1953-gf0fb6b6acd805c -->
    The support for Concepts TS was removed.  <code>-fconcepts-ts</code> has no
    effect anymore.
  </li>
  <li>
    <!-- commit r15-2774-g596d1ed9d40b10 -->
    A new option
    <a href="https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Dialect-Options.html#index-Wtemplate-body">
    <code>-Wtemplate-body</code></a> was added, which can be used to disable
    diagnosing errors when parsing a template.
  </li>
  <li>
    C++ Modules have been greatly improved.
  </li>
  <li>
    <!-- commit r15-3433-g3775f71c8909b3 -->
    C++11 attributes are now supported even in C++98.
  </li>
  <li>
    New
    <a href="https://gcc.gnu.org/onlinedocs/gcc/Common-Type-Attributes.html#index-flag_005fenum-type-attribute">
    <code>flag_enum</code></a> attribute to indicate that the enumerators
    are used in bitwise operations; this suppresses a <code>-Wswitch</code>
    warning.
  </li>
  <li>
    The <a href="https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Dialect-Options.html#index-Wdangling-reference">
    <code>-Wdangling-reference</code></a> warning has been improved: for
    example, it doesn't warn for empty classes anymore.
  </li>
  <li>
    The front end's handling of explicitly-defaulted functions has been
    corrected to properly handle <em>[dcl.fct.def.default]</em>.  The
    new
    <a href="https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Dialect-Options.html#index-Wdefaulted-function-deleted">
    <code>-Wdefaulted-function-deleted</code></a> warning warns when an
    explicitly defaulted function is deleted.
  </li>
  <li>
    The implementation of <a href="https://wg21.link/cwg2789">DR 2789</a>
    was refined.
  </li>
  <li>
    <!-- commit r15-4050-g5dad738c1dd164 -->
    Compilation time speed ups, e.g. by improving hashing of template
    specializations.
  </li>
  <li>
    <!-- commit r15-5109-g417b4cc9bf2180 -->
    Support for <code>__builtin_operator_new</code> and
    <code>__builtin_operator_delete</code> was added.  See
    <a href="https://gcc.gnu.org/onlinedocs/gcc/New_002fDelete-Builtins.html">
    the manual</a> for more info.
  </li>
  <li>
    <!-- commit r15-6052-g12de1942a0a673 -->
    More prvalues are evaluated at compile time
    (<a href="https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=12de1942a0a673f9f2f1c2bfce4279a666061ffc">git</a>).
  </li>
  <li>
    Various diagnostic improvements.
  </li>
</ul>
<h4 id="libstdcxx">Runtime Library (libstdc++)</h4>

<ul>
  <li>Debug assertions are now enabled by default for unoptimized builds.
    Use <code>-D_GLIBCXX_NO_ASSERTIONS</code> to override this.
  </li>
  <li>Improved experimental support for C++26, including:
    <ul>
    <li><code>views::concat</code>.</li>
    <li>Member <code>visit</code>.</li>
    <li>Type-checking <code>std::format</code> args.</li>
    </ul>
  </li>
  <li>Improved experimental support for C++23, including:
    <ul>
    <li>
      <code>std</code> and <code>std.compat</code> modules
      (also supported for C++20).
    </li>
    <li>
      <code>std::flat_map</code> and <code>std::flat_set</code>.
    </li>
    <li>
      Clarify handling of encodings in localized formatting of chrono types.
    </li>
    </ul>
  </li>
</ul>

<!-- <h3 id="d">D</h3> -->

<h3 id="fortran">Fortran</h3>

<ul>
  <li>Fortran 2023: The <code>selected_logical_kind</code> intrinsic function
      and, in the <code>ISO_FORTRAN_ENV</code> module, the named constants
      <code>logical{8,16,32,64}</code> and <code>real16</code> were added.</li>
  <li>Experimental support for <code>unsigned</code> integers, enabled by
      <code>-funsigned</code>; see <a
      href="https://gcc.gnu.org/onlinedocs/gfortran/Unsigned-integers.html"
      >gfortran documentation</a> for details. These have been proposed
      (<a href="https://j3-fortran.org/doc/year/24/24-116.txt">J3/24-116</a>)
      for inclusion in the next Fortran standard.</li>
  <li>Missing commas separating descriptors in input/output format strings are no
      longer permitted by default and are rejected at run-time unless -std=legacy
      is used when compiling the main program unit.  See Fortran 2023 constraint C1302.
  </li>
  <li>The Fortran module <code>*.mod</code> format generated by GCC 15 is
      incompatible with the module format generated by GCC 8 - 14, but GCC
      15 can for compatibility still read GCC 8 - 14 created module
      files.</li>
</ul>

<!-- <h3 id="go">Go</h3> -->

<!-- .................................................................. -->
<!-- <h2 id="jit">libgccjit</h2> -->

<!-- .................................................................. -->
<h2 id="targets">New Targets and Target Specific Improvements</h2>

<!-- <h3 id="aarch64">AArch64</h3> -->

<h3 id="amdgcn">AMD GPU (GCN)</h3>

<ul>
  <li>Experimental support for supporting generic devices has been added;
    specifying <code>gfx9-generic</code>, <code>gfx10-3-generic</code>,
    or <code>gfx11-generic</code> to
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AMD-GCN-Options.html">
    <code>-march=</code></a> will generate code that can run on all
    devices of a series.  Additionally, the following specific devices
    are now have experimental support, all of which are compatible with a
    listed generic: <code>gfx902</code>, <code>gfx904</code>,
    <code>gfx909</code>, <code>gfx1031</code>, <code>gfx1032</code>,
    <code>gfx1033</code>, <code>gfx1034</code>, <code>gfx1035</code>,
    <code>gfx1101</code>, <code>gfx1102</code>, <code>gfx1150</code>,
    and <code>gfx1151</code>.  To use any of the listed new devices including
    the generic ones, GCC has to be configured to build the runtime library
    for the device. Note that generic support requires a future ROCm (newer
    than 6.3.2). For details, consult GCC's
    <a href="https://gcc.gnu.org/install/specific.html#amdgcn-x-amdhsa">
    installation notes</a>.</li>
  <li>Support for Fiji (gfx803) devices has been removed (this was already
    deprecated in GCC 14).</li>
</ul>

<!-- <h3 id="arc">ARC</h3> -->

<!-- <h3 id="arm">arm</h3> -->

<h3 id="avr">AVR</h3>

<ul>
  <li>Support has been added for the <code>signal</code> and <code>interrupt</code>
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Function-Attributes.html#index-signal_0028num_0029-function-attribute_002c-AVR"
       >function attributes</a>
    that allow to specify the interrupt vector number as an argument.
    It allows to use static functions as interrupt handlers, and also
    functions defined in a C++ namespace.</li>
  <li>Support has been added for the <code>noblock</code> function attribute.
    It can be specified together with the <code>signal</code> attribute to
    indicate that the interrupt service routine should start with a
    <code>SEI</code> instruction to globally re-enable interrupts.
    The difference to the <code>interrupt</code> attribute is that the
    <code>noblock</code> attribute just acts like a flag and does not
    impose a specific function name.</li>
  <li>Support has been added for the <code>__builtin_avr_mask1</code>
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Built-in-Functions.html#index-_005f_005fbuiltin_005favr_005fmask1"
       >built-in function</a>.  It can be used to compute some bit masks when
    code like <code>1&nbsp;&lt;&lt;&nbsp;offset</code> is not fast enough.</li>
  <li>Support has been added for a new 24-bit
    <a href="https://gcc.gnu.org/onlinedocs/gcc/Named-Address-Spaces.html#AVR-Named-Address-Spaces-1"
       >named address space</a> <code>__flashx</code>.
    It is similar to the <code>__memx</code> address space introduced in v4.7,
    but reading is a bit more efficient since it only supports reading from
    program memory. Objects in the <code>__flashx</code> address space are
    located in the <code>.progmemx.data</code> section.</li>
  <li>Apart from the built-in types <code>__int24</code> and
    <code>__uint24</code> supported since v4.7, support has been added for the
    <code>signed __int24</code> and <code>unsigned __int24</code> types.</li>
  <li>Code generation for the 32-bit integer shifts with constant
    offset has been improved. The code size may slightly increase even
    when optimizing for code size with <code>-Os</code>.</li>
  <li>Support has been added for a <em>compact vector table</em> as supported
    by some AVR devices.  It can be activated by the new command-line option
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html#index-mcvt"
       ><code>-mcvt</code></a>.
    It links <code>crt<i>mcu</i>-cvt.o</code> as startup code which
    is supported since AVR-LibC v2.3.</li>
  <li>New AVR specific optimization passes have been added.
    They run after register allocation and can be controlled by the new
    command-line options
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html#index-mfuse-move"
       ><code>-mfuse-move</code></a>,
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html#index-msplit-ldst"
       ><code>-msplit-ldst</code></a> and
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html#index-msplit-bit-shift"
       ><code>-msplit-bit-shift</code></a>.</li>
  <li>Support has been added for the new option
    <a href="https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html#index-mno-call-main"
       ><code>-mno-call-main</code></a>.  Instead of calling <code>main</code>,
    it will be located in section <code>.init9</code>.</li>
</ul>

<h3 id="x86">IA-32/x86-64</h3>

<ul>
  <li>New ISA extension support for Intel AMX-AVX512 was added.
      AMX-AVX512 intrinsics are available via the <code>-mamx-avx512</code>
      compiler switch.
  </li>
  <li>New ISA extension support for Intel AMX-FP8 was added.
      AMX-FP8 intrinsics are available via the <code>-mamx-fp8</code>
      compiler switch.
  </li>
  <li>New ISA extension support for Intel AMX-MOVRS was added.
      AMX-MOVRS intrinsics are available via the <code>-mamx-movrs</code>
      compiler switch.
  </li>
  <li>New ISA extension support for Intel AMX-TF32 was added.
      AMX-TF32 intrinsics are available via the <code>-mamx-tf32</code>
      compiler switch.
  </li>
  <li>New ISA extension support for Intel AMX-TRANSPOSE was added.
      AMX-TRANSPOSE intrinsics are available via the <code>-mamx-transpose</code>
      compiler switch.
  </li>
  <li>New ISA extension support for Intel AVX10.2 was added.
      AVX10.2 intrinsics are available via the <code>-mavx10.2</code> or
      <code>-mavx10.2-256</code> compiler switch with 256-bit vector size
      support. 512-bit vector size support for AVX10.2 intrinsics are
      available via the <code>-mavx10.2-512</code> compiler switch.
  </li>
  <li>New ISA extension support for Intel MOVRS was added.
      MOVRS intrinsics are available via the <code>-mmovrs</code>
      compiler switch. 128- and 256- bit MOVRS intrinsics are available via
      the <code>-mmovrs -mavx10.2</code> compiler switch. 512-bit MOVRS
      intrinsics are available via the <code>-mmovrs -mavx10.2-512</code>
      compiler switch.
  </li>
  <li>EVEX version support for Intel SM4 was added.
      New 512-bit SM4 intrinsics are available via the
      <code>-msm4 -mavx10.2-512</code> compiler switch.
  </li>
  <li>GCC now supports the Intel CPU named Diamond Rapids through
    <code>-march=diamondrapids</code>.
    Based on Granite Rapids, the switch further enables the AMX-AVX512,
    AMX-FP8, AMX-MOVRS, AMX-TF32, AMX-TRANSPOSE, APX_F, AVX10.2 with 512-bit
    support, AVX-IFMA. AVX-NE-CONVERT, AVX-VNNI-INT16, AVX-VNNI-INT8,
    CMPccXADD, MOVRS, SHA512, SM3, SM4 and USER_MSR ISA extensions.
  </li>
  <li>Support for Xeon Phi CPUs (a.k.a. Knight Landing and Knight Mill) were
      removed in GCC 15. GCC will no longer accept <code>-march=knl</code>,
      <code>-march=knm</code>, <code>-mavx5124fmaps</code>,
      <code>-mavx5124vnniw</code>, <code>-mavx512er</code>,
      <code>-mavx512pf</code>, <code>-mprefetchwt1</code>,
      <code>-mtune=knl</code>, and <code>-mtune=knm</code> compiler switches.
  </li>
  <li>With the <code>-mveclibabi</code> compiler switch GCC is able to generate
    vectorized calls to external libraries. GCC 15 newly supports generating
    vectorized math calls to the math library from AMD Optimizing CPU Libraries
    (AOCL LibM). This option is available through
    <code>-mveclibabi=aocl</code>. GCC still supports generating calls to AMD
    Core Math Library (ACML). However, that library is end-of-life and AOCL
    offers many more vectorized functions.
  </li>
</ul>

<!-- <h3 id="mips">MIPS</h3> -->

<!-- <h3 id="mep">MeP</h3> -->

<!-- <h3 id="msp430">MSP430</h3> -->

<!-- <h3 id="nds32">NDS32</h3> -->

<!-- <h3 id="nios2">Nios II</h3> -->

<h3 id="nvptx">NVPTX</h3>

<ul>
  <li>GCC's nvptx target now supports constructors and destructors.
      For this, a recent version of <a
      href="https://gcc.gnu.org/install/specific.html#nvptx-x-none"
      >nvptx-tools is required</a>.</li>
</ul>

<!-- <h3 id="hppa">PA-RISC</h3> -->

<!-- <h3 id="powerpc">PowerPC / PowerPC64 / RS6000</h3> -->

<!-- <h3 id="s390">S/390, System z, IBM z Systems</h3> -->

<!-- <h3 id="riscv">RISC-V</h3> -->

<!-- <h3 id="rx">RX</h3> -->

<h3 id="sh">SH</h3>

<ul>
  <li>Bare metal <code>sh-elf</code> targets are now using the newer soft-fp
      library for improved performance of floating-point emulation on CPUs
      without hardware floating-point support.</li>
</ul>

<!-- <h3 id="sparc">SPARC</h3> -->

<!-- <h3 id="Tile">Tile</h3> -->

<!-- .................................................................. -->
<h2 id="os">Operating Systems</h2>

<!-- <h3 id="aix">AIX</h3> -->

<h3>PowerPC Darwin</h3>
<ul>
  <li>Fortran's IEEE modules are now suppored on Darwin PowerPC.</li>
</ul>

<!-- <h3 id="fuchsia">Fuchsia</h3> -->

<!-- <h3 id="dragonfly">DragonFly BSD</h3> -->

<!-- <h3 id="freebsd">FreeBSD</h3> -->

<!-- <h3 id="gnulinux">GNU/Linux</h3> -->

<!-- <h3 id="rtems">RTEMS</h3> -->

<!-- <h3 id="solaris">Solaris</h3> -->

<!-- <h3 id="vxmils">VxWorks MILS</h3> -->

<!-- <h3 id="windows">Windows</h3> -->


<!-- .................................................................. -->
<!-- <h2>Documentation improvements</h2> -->

<!-- .................................................................. -->
<!-- <h2 id="plugins">Improvements for plugin authors</h2> -->

<!-- .................................................................. -->
<h2>Other significant improvements</h2>

<!-- <h3 id="uninitialized">Eliminating uninitialized variables</h3> -->

<!-- .................................................................. -->
<!-- <h2 id="15.1">GCC 15.1</a></h2> -->

</body>
</html>
